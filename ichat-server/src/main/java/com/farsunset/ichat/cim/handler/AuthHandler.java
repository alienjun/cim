 
package com.farsunset.ichat.cim.handler;


import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import com.farsunset.cim.nio.constant.CIMConstant;
import com.farsunset.cim.nio.handle.CIMRequestHandler;
import com.farsunset.cim.nio.mutual.Message;
import com.farsunset.cim.nio.mutual.ReplyBody;
import com.farsunset.cim.nio.mutual.SentBody;
import com.farsunset.cim.nio.session.DefaultSessionManager;
import com.farsunset.ichat.common.util.ContextHolder;

/**
 * 鉴权实现,真实使用时可在这里 进行账号密码验证
 * 
 * @author
 */
public class AuthHandler implements CIMRequestHandler {

	protected final Logger logger = Logger.getLogger(AuthHandler.class);

	public ReplyBody process(IoSession newSession, SentBody message) {

		
		ReplyBody reply = new ReplyBody();
		DefaultSessionManager sessionManager= ((DefaultSessionManager) ContextHolder.getBean("defaultSessionManager"));
		String account = message.get("account");
		try {
		 
			newSession.setAttribute("channel", message.get("channel"));
			newSession.setAttribute("deviceId", message.get("deviceId"));	
			newSession.setAttribute("loginTime", System.currentTimeMillis());	
			
			 /**
			 * 由于客户端断线服务端可能会无法获知的情况，客户端重连时，需要关闭旧的连接
			 */
			IoSession oldSession  = sessionManager.getSession(account);
			 
			if(oldSession!=null )
			{
				oldSession.removeAttribute("account");
				//如果是账号已经在另一台终端登录。则让另一个终端下线
				if(oldSession.getAttribute("deviceId")!=null&&!oldSession.getAttribute("deviceId").equals(newSession.getAttribute("deviceId")))
				{
					Message msg = new Message();
					msg.setType("999");//强行下线消息类型
					oldSession.write(msg);
				} 
				
				oldSession.close(true);
			}

			
			sessionManager.addSession(account, newSession);
				
				 
			reply.setCode(CIMConstant.ReturnCode.CODE_200);
		} catch (Exception e) {
			reply.setCode(CIMConstant.ReturnCode.CODE_500);
			e.printStackTrace();
		}
		logger.debug("auth :account:" +message.get("account")+"-----------------------------" +reply.getCode());
		return reply;
	}
	
}